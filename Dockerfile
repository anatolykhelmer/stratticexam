FROM php:7.4-apache

RUN apt-get update \
  && apt-get install unzip zip \
  && a2enmod rewrite \
  && sed -i 's!/var/www/html!/var/www/app/public!g' /etc/apache2/sites-available/000-default.conf \
  && curl -sS https://getcomposer.org/installer \
      | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/app