<?php
namespace StratticTest;

use PHPUnit\Framework\TestCase;
use Strattic\LandFinder;

class LandFinderTest extends TestCase
{
    public function testCountLands()
    {
        $matrix = [];
        $matrix[] = [0, 1, 0];
        $matrix[] = [1, 1, 0];
        $matrix[] = [0, 0, 1];
        $matrix[] = [1, 0, 1];

        $sut = new LandFinder($matrix);

        $this->assertEquals(3, $sut->countLands());
    }
}
