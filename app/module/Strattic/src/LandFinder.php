<?php
namespace Strattic;

class LandFinder
{
    private $matrix;
    private $touched;
    private $xSize;
    private $ySize;

    public function __construct($matrix)
    {
        $this->matrix = $matrix;
        $this->setUp();
    }

    private function setUp()
    {
        $this->xSize = count($this->matrix);
        $this->ySize = count($this->matrix[0]);

        $this->touched = [];
    }

    public function countLands()
    {
        $counter = 0;
        for ($i = 0; $i < $this->xSize; $i++) {
            for ($j = 0; $j < $this->ySize; $j++) {
                $cell = new Cell($i, $j);
                if ($this->isTouched($cell)) {
                    continue;
                }

                if ($this->isGround($cell)) {
                    $counter++;
                    $this->touchNeighbours($cell);
                }
            }
        }

        return $counter;
    }

    private function touchNeighbours(Cell $cell)
    {
        if ($leftCell = $this->getLeft($cell)) {
            $this->process($leftCell);
        }

        if ($rightCell = $this->getRight($cell)) {
            $this->process($rightCell);
        }

        if ($topCell = $this->getTop($cell)) {
            $this->process($topCell);
        }

        if ($bottomCell = $this->getBottom($cell)) {
            $this->process($bottomCell);
        }
    }

    private function touch(Cell $cell)
    {
        $this->touched[$cell->x][$cell->y] = true;
    }

    private function isTouched(Cell $cell)
    {
        return isset($this->touched[$cell->x][$cell->y]);
    }

    private function getLeft(Cell $cell)
    {
        if ($cell->x > 0) {
            return new Cell($cell->x - 1, $cell->y);
        }
    }

    private function getRight(Cell $cell)
    {
        if ($cell->x < $this->xSize - 1) {
            return new Cell($cell->x + 1, $cell->y);
        }
    }

    private function getTop(Cell $cell)
    {
        if ($cell->y > 0) {
            return new Cell($cell->x, $cell->y - 1);
        }
    }

    private function getBottom(Cell $cell)
    {
        if ($cell->y < $this->ySize - 1) {
            return new Cell($cell->x, $cell->y + 1);
        }
    }

    private function process(Cell $cell)
    {
        if (! $this->isGround($cell)) {
            return;
        }
        if (! $this->isTouched($cell)) {
            $this->touch($cell);
            $this->touchNeighbours($cell);
        }
    }

    private function isGround(Cell $cell)
    {
        return $this->matrix[$cell->x][$cell->y];
    }

//    function touchNeighbours($x, $y, $matrix, &$touched) {
//        $xsize = count($matrix);
//        $ysize = count($matrix[0]);
//        // left
//        if ($x > 0) {
//            if ($matrix[$x - 1][$y]) {
//                if (! $touched[$x - 1][$y]) {
//                    $touched[$x - 1][$y] = true;
//                    touchNeighbours($x - 1, $y, $matrix, $touched);
//                }
//            }
//        }
//
//        // right
//        if ($x < $xsize - 1) {
//            if ($matrix[$x + 1][$y]) {
//                if (! $touched[$x + 1][$y]) {
//                    $touched[$x + 1][$y] = true;
//                    touchNeighbours($x + 1, $y, $matrix, $touched);
//                }
//            }
//        }
//
//        // top
//        if ($y > 0) {
//            if ($matrix[$x][$y - 1]) {
//                if (! $touched[$x][$y - 1]) {
//                    $touched[$x][$y - 1] = true;
//                    touchNeighbours($x, $y - 1, $matrix, $touched);
//                }
//            }
//        }
//
//        // bottom
//        if ($y < $ysize - 1) {
//            if ($matrix[$x][$y + 1]) {
//                if (! $touched[$x][$y + 1]) {
//                    $touched[$x][$y + 1] = true;
//                    touchNeighbours($x, $y + 1, $matrix, $touched);
//                }
//            }
//        }
//    }
}
