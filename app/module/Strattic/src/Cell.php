<?php
namespace Strattic;

class Cell
{
    public $x, $y;

    /**
     * Cell constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
}
